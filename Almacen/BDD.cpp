#include "BDD.hpp"

namespace TodoSQL{

BDD::BDD(MYSQL_RES*zr):ms_r(zr){
}
BDD::~BDD(){
}

/** CREA UNA NUEVA TABLA
 * @param char* Nombre de la tabla a crear
 * @return 0=OK / !=ERR
 */
Tabla*BDD::NewTabla(char*znomtbl){
}

/** DEVUELVE LA TABLA
 * @param char* Nombre de la tabla
 * @return 0=OK / !=ERR
 */
Tabla*BDD::GetTabla(char*znomtbl){
  return 0;
}


/** BORRA LA TABLA
 * @param char* Nombre de la tabla
 * @return 0=OK / !=ERR
 */
void BDD::DelTabla(char*znomtbl){
  return 0;
}


/** DEVUELVE LA TABLA DESDE UN COMANDO SQL
 * @param char* Comando SQL
 * @return 0=OK / !=ERR
 * -1 - Sin memoria
 */
Tabla*BDD::GetTabla(char*zcom){
  char*str;sprintf(str,"SELECT * FROM %s",zcom);
  if(!str)return -1;
  free(str);
  return 0;
}

}
