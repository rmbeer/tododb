#ifndef __ALMACEN_MYISAM_H__
#define __ALMACEN_MYISAM_H__
#include "../../tabla.hpp"

namespace TodoDB{
namespace Almacen{
namespace MyISAM{

class tabla:public ::tabla{
public:
  MyISAM();
  ~MyISAM();
  //Filas
  virtual int AddFila();
  virtual int GetFila();
  virtual int JmpFila(Ulong);
  virtual Ulong GetTFila();
  //Puntero
  virtual int RegPrev();
  virtual int RegNext();
  virtual int RegFirst();
  virtual int RegLast();
  //Valor interno
  void*MiInfo;
  char*buf;
  int ind;
};

}}}

#endif
