#include <myisam.h>
#include "../../../mariadb/storage/myisam/mi_rename.c"
#include "BDD.hpp"

namespace TodoDB{
namespace Almacen{
namespace MyISAM{

BDD::BDD(MYSQL_RES*zr):ms_r(zr){
}
BDD::~BDD(){
}

/** ABRIR ARCHIVO DE BD
 * @param char* Nombre Archivo
 * @return 0=OK / !=ERR
 * ERR:
 * -1 - Imposible abrir archivo
 */
int BDD::OpenF(char*zna){
  int modo=O_RDWR;
  /* LISTA DE FLAGS:
   * HA_OPEN_INTERNAL_TABLE
   * HA_OPEN_FROM_SQL_LAYER
   * HA_OPEN_TMP_TABLE
   * HA_OPEN_WAIT_IF_LOCKED
   * HA_OPEN_IGNORE_IF_LOCKED
   * HA_OPEN_FOR_REPAIR
   * HA_OPEN_ABORT_IF_CRASHED
   * HA_OPEN_TMP_TABLE
   * HA_OPEN_MMAP
   * HA_OPEN_DELAY_KEY_WRITE
   */
  Uint flag=0;
  st_myisam_info*minf;
  minf=mi_open(zna,modo,flag);
  if(!minf)return -1;
  MiInfo=minf;
  return 0;
}

int BDD::NewOpenF(char*zna){
  mi_create(zna,tkeys,keys,tcol,col,tunique,unique,
  return 0;
}

void BDD::CloseF(){
  st_myisam_info*minf=MiInfo;
  mi_close(minf);
}

/** CREA UNA NUEVA TABLA
 * @param char* Nombre de la tabla a crear
 * @return 0=OK / !=ERR
 */
Tabla*BDD::NewTabla(char*znomtbl){
}

/** DEVUELVE LA TABLA
 * @param char* Nombre de la tabla
 * @return 0=OK / !=ERR
 */
Tabla*BDD::GetTabla(char*znomtbl){
  return 0;
}


/** BORRA LA TABLA
 * @param char* Nombre de la tabla
 * @return 0=OK / !=ERR
 */
void BDD::DelTabla(char*znomtbl){
  return 0;
}


/** DEVUELVE LA TABLA DESDE UN COMANDO SQL
 * @param char* Comando SQL
 * @return 0=OK / !=ERR
 * -1 - Sin memoria
 */
Tabla*BDD::GetTabla(char*zcom){
  return 0;
}

int BDD::RenTabla(char*ori,char*des){
  return mi_rename(ori,des);
}

}}}
