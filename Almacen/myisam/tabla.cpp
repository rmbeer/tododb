#include <myisam.h>
#include <mi_rprev.c>
#include "../../../mariadb/storage/myisam/mi_rnext.c"
#include "../../../mariadb/storage/myisam/mi_rfirst.c"
#include "../../../mariadb/storage/myisam/mi_rlast.c"
#include "api.hpp"

namespace TodoDB{
namespace Almacen{
namespace MyISAM{

tabla::tabla(){
}
tabla::~tabla(){
}

int AddFila(){
  return 0;
}

int GetFila(){
  return 0;
}

int JmpFila(Ulong nfil){
  return 0;
}

Ulong GetTFila(){
  return 0;
}

//============================================================================//
// PUNTEROS                                                                   //
//============================================================================//

/** ANTERIOR REGISTRO
 * Al registro con la misma clave
 */
int Tabla::RegPrev(){
  st_myisam_info*minf=(st_myisam_info*)MiInfo;
  return mi_rprev(minf,buf,ind);
}

/** SIGUIENTE REGISTRO
 * Al registro con la misma clave
 * ERROR:
 * HA_ERR_KEY_NOT_FOUND
 * HA_ERR_END_OF_FILE
 */
int Tabla::RegNext(){
  st_myisam_info*minf=(st_myisam_info*)MiInfo;
  return mi_rnext(minf,buf,ind);
}

/** PRIMER REGISTRO
 * Al registro con la misma clave
 */
int Tabla::RegFirst(){
  st_myisam_info*minf=(st_myisam_info*)MiInfo;
  return mi_rfirst(minf,buf,ind);
}

/** ULTIMO REGISTRO
 * Al registro con la misma clave
 */
int Tabla::RegLast(){
  st_myisam_info*minf=(st_myisam_info*)MiInfo;
  return mi_rlast(minf,buf,ind);
}

}}}
