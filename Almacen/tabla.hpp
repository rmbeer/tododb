#ifndef __TODOSQL_TABLA_H___
#define __TODOSQL_TABLA_H___
#include "../Base.h"

namespace TodoDB{
namespace Almacen{

class Tabla{
public:
  Tabla();
  ~Tabla();
  //Campos
  //Fila temporal
  //Filas
  virtual int AddFila();
  virtual int AddFila(Uchar,...);
  virtual int SetFila();
  virtual int SetFila(Uchar,...);
  virtual int GetFila();
  virtual int JmpFila(Ulong);
  virtual Ulong GetTFila();
  virtual Uchar GetTCol();
  //Puntero
  virtual int RegPrev();
  virtual int RegNext();
  virtual int RegFirst();
  virtual int RegLast();
private:
  Uchar tCampo;
};

}}

#endif
