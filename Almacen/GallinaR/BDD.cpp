#include <myisam.h>
#include "../../../mariadb/storage/myisam/mi_rename.c"
#include "BDD.hpp"

namespace TodoDB{
namespace Almacen{
namespace MyISAM{

BDD::BDD(){
  tReg=0;tCampo=0;Campo=0;nomarch=0;
}
BDD::~BDD(){_Free();
  if(nomarch)free(nomarch);
}

/** ABRIR ARCHIVO DE BD
 * Para este almacenamiento se debe omitir la Extension
 * @param char* Nombre Archivo
 * @return 0=OK / !=ERR
 * ERR:
 * -1 - Imposible abrir archivo
 */
int BDD::OpenF(char*zna){int i;
  return 0;
}

int BDD::NewOpenF(char*zna){
  return 0;
}

void BDD::CloseF(){
}

/** CREA UNA NUEVA TABLA
 * @param char* Nombre de la tabla a crear
 * @return 0=OK / !=ERR
 */
Tabla*BDD::NewTabla(char*znomtbl){
}

/** DEVUELVE LA TABLA
 * @param char* Nombre de la tabla
 * @return 0=OK / !=ERR
 */
Tabla*BDD::GetTabla(char*znomtbl){
  return 0;
}


/** BORRA LA TABLA
 * @param char* Nombre de la tabla
 * @return 0=OK / !=ERR
 */
void BDD::DelTabla(char*znomtbl){
  return 0;
}

}}}
