#ifndef __TODOSQL_BDD_H__
#define __TODOSQL_BDD_H__
#include <mysql.h>

namespace TodoDB{
namespace Almacen{
namespace MyISAM{

class BDD{
public:
  BDD(MYSQL_RES*);
  ~BDD();
  //Tabla
  Tabla*NewTabla();
  Tabla*GetTabla(char*);
  void DelTabla(char*);
  Tabla*GetTablaSQL(char*);
  int RenTabla(char*,char*);
private:
};

}}}

#endif
