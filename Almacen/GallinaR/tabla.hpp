#ifndef __ALMACEN_MYISAM_H__
#define __ALMACEN_MYISAM_H__
#include "../tabla.hpp"

namespace TodoDB{
namespace Almacen{
namespace GallinaR{

class Tabla:public TodoDB::Almacen::Tabla{
public:
  Tabla();
  ~Tabla();
  void _Free();
  //Open
  int OpenF(const char*);
  int SaveF();
  void CloseF();
  //Filas
  virtual int AddFila();
  virtual int AddFila(Uchar,...);
  virtual int SetFila();
  virtual int SetFila(Uchar,...);
  virtual int GetFila();
  virtual int JmpFila(Ulong);
  virtual Ulong GetTFila();
  virtual Uchar GetTCol(){return tCampo;}
  //Puntero
  virtual int RegPrev();
  virtual int RegNext();
  virtual int RegFirst();
  virtual int RegLast();
  //Datos
  int DSet(Uchar,const void*);
  int DSetn(const char*,const void*);
  int DSet(Uchar,long);
  int DSetn(const char*,long);
  Ulong DGetN(Ulong,Uchar);
  Ulong DGetNn(Ulong,const char*);
  char*DGetS(Ulong,Uchar);
  char*DGetSn(Ulong,const char*);
  //Campos
  int CAdd(const char*,Uchar,Uint,const char*);
  //Valor interno
private:
  char*nomarch;
  struct _STR_Campo{
    char*n;char tp;
    Uint tam;
    char*desc;
  }*Campo;
  Uchar tCampo;
  Uint offCampo[256];
  char*Reg[1000];
  Uint tReg,tamReg;
  Uint ptrReg; //Puntero Fila
  char*TempReg; //Fila temporal
  Uint ptrTempReg; //Puntero de Fila Original
};

}}}

#endif
