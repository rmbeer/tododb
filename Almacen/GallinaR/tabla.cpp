#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tabla.hpp"

namespace TodoDB{
namespace Almacen{
namespace GallinaR{

Tabla::Tabla(){
  tReg=0;tCampo=0;Campo=0;nomarch=0;TempReg=0;ptrTempReg=-1;
}
Tabla::~Tabla(){_Free();
  if(nomarch)free(nomarch);
}

int Tabla::AddFila(){
  if(tReg>=1000)return -1;
  Reg[tReg]=(char*)malloc(tamReg);
  memcpy(Reg[tReg],TempReg,tamReg);tReg++;
  return 0;
}

int Tabla::AddFila(Uchar zprm,...){Uint i;
  va_list zlp;
  va_start(zlp,(char)zprm);
  long f;
  for(i=zprm;i<tCampo;i++){
    switch(Campo[i].tp){
    case 0:case 3:f=va_arg(zlp,long);memcpy(TempReg+offCampo[i],&f,4);break;
    case 1:f=va_arg(zlp,int);memcpy(TempReg+offCampo[i],&f,1);break;
    case 2:f=va_arg(zlp,int);memcpy(TempReg+offCampo[i],&f,2);break;
    case 4:memcpy(TempReg+offCampo[i],va_arg(zlp,char*),Campo[i].tam);break;
    //case 5:memcpy(TempReg+offCampo[i],va_arg(zlp,char*),8);break;
    }
  }
  va_end(zlp);AddFila();
  return 0;
}

int Tabla::SetFila(){
  memcpy(Reg[ptrTempReg],TempReg,tamReg);
  return 0;
}

int Tabla::SetFila(Uchar zprm,...){Uint i;
  va_list zlp;
  va_start(zlp,zprm);
  long f;
  for(i=zprm;i<tCampo;i++){
    switch(Campo[i].tp){
    case 0:case 3:f=va_arg(zlp,long);memcpy(TempReg+offCampo[i],&f,4);break;
    case 1:f=va_arg(zlp,int);memcpy(TempReg+offCampo[i],&f,1);break;
    case 2:f=va_arg(zlp,int);memcpy(TempReg+offCampo[i],&f,2);break;
    case 4:memcpy(TempReg+offCampo[i],va_arg(zlp,char*),Campo[i].tam);break;
    //case 5:memcpy(TempReg+offCampo[i],va_arg(zlp,char*),8);break;
    }
  }
  va_end(zlp);SetFila();
  return 0;
}

int Tabla::GetFila(){
  if(ptrReg==tReg){bzero(TempReg,tamReg);
  }else{memcpy(TempReg,Reg[ptrReg],tamReg);}
  ptrTempReg=ptrReg;
  return 0;
}

int Tabla::JmpFila(Ulong nfil){
  if(nfil>tReg)return -1;
  ptrReg=nfil;return 0;
}

Ulong Tabla::GetTFila(){return tReg&0x7FFF;}

//============================================================================//
// PUNTEROS                                                                   //
//============================================================================//

/** ANTERIOR REGISTRO
 * Al registro con la misma clave
 */
int Tabla::RegPrev(){
  if(ptrReg>0)ptrReg--;
  return 0;
}

/** SIGUIENTE REGISTRO
 * Al registro con la misma clave
 * ERROR:
 * HA_ERR_KEY_NOT_FOUND
 * HA_ERR_END_OF_FILE
 */
int Tabla::RegNext(){
  if(ptrReg<tReg)ptrReg++;
  return 0;
}

/** PRIMER REGISTRO
 * Al registro con la misma clave
 */
int Tabla::RegFirst(){
  ptrReg=0;
  return 0;
}

/** ULTIMO REGISTRO
 * Al registro con la misma clave
 */
int Tabla::RegLast(){
  ptrReg=tReg;
  return 0;
}

//============================================================================//
// DATOS                                                                      //
//============================================================================//

/* TODO: No puede sobrecargar Uchar con char* ??? */

int Tabla::DSet(Uchar zncol,const void*zdat){
  if(ptrTempReg==65535)return -1;
  char*ztmp=TempReg+offCampo[zncol];
  switch(Campo[zncol].tp){
  case 0:case 3:memcpy(ztmp,zdat,4);break;
  case 1:memcpy(ztmp,zdat,1);break;
  case 2:memcpy(ztmp,zdat,2);break;
  case 4:memcpy(ztmp,zdat,Campo[zncol].tam);break;
  case 5:memcpy(ztmp,zdat,8);break;
  }
  return 0;
}
int Tabla::DSetn(const char*zncol,const void*zdat){Uint i;
  for(i=0;i<tCampo;i++)if(!strcmp(Campo[i].n,zncol))return DSet(i,zdat);
  return -1;
}
int Tabla::DSet(Uchar zncol,long znum){return DSet(zncol,&znum);}
int Tabla::DSetn(const char*zncol,long znum){return DSetn(zncol,&znum);}

Ulong Tabla::DGetN(Ulong zf,Uchar znc){
  switch(Campo[znc].tp){
  case 1:return Reg[zf][offCampo[znc]];
  case 2:return *((Uint*)(Reg[zf]+offCampo[znc]));
  case 3:return *((Ulong*)(Reg[zf]+offCampo[znc]));
  }
  return 0;
}
Ulong Tabla::DGetNn(Ulong zf,const char*znc){Uchar i;
  for(i=0;i<tCampo;i++)if(!strcmp(Campo[i].n,znc))return DGetN(zf,i);
  return 0;
}

char*Tabla::DGetS(Ulong zf,Uchar znc){
  switch(Campo[znc].tp){
  case 4:return Reg[zf]+offCampo[znc];
  }
  return 0;
}
char*Tabla::DGetSn(Ulong zf,const char*znc){Uchar i;
  for(i=0;i<tCampo;i++)if(!strcmp(Campo[i].n,znc))return DGetS(zf,i);
  return 0;
}

void Tabla::_Free(){int i;
  if(TempReg)free(TempReg);TempReg=0;
  if(Campo){for(i=0;i<tCampo;i++){
    free(Campo[i].n);free(Campo[i].desc);}free(Campo);}Campo=0;
  for(i=0;i<tReg;i++)free(Reg[i]);tReg=0;tCampo=0;
}

//============================================================================//
// CAMPOS                                                                     //
//============================================================================//

int Tabla::CAdd(const char*zn,Uchar ztp,Uint ztam,const char*zd){
  _STR_Campo*ctmp=(_STR_Campo*)malloc(sizeof(_STR_Campo)*(tCampo+1));
  memcpy(ctmp,Campo,sizeof(_STR_Campo)*tCampo);
  //bzero(ctmp+tCampo,sizeof(_STR_Campo));
  ctmp[tCampo].n=(char*)malloc(strlen(zn)+1);
  ctmp[tCampo].desc=(char*)malloc(strlen(zd)+1);
  strcpy(ctmp[tCampo].n,zn);strcpy(ctmp[tCampo].desc,zd);
  ctmp[tCampo].tp=ztp;ctmp[tCampo].tam=ztam;
  free(Campo);Campo=ctmp;offCampo[tCampo]=tamReg;
  tCampo++;Uint ztamReg=tamReg;
  switch(ztp){
  case 0:case 3:tamReg+=4;break;
  case 1:tamReg+=1;break;
  case 2:tamReg+=2;break;
  case 4:tamReg+=ztam;break;
  case 5:tamReg+=8;break;
  }
  // Arregla todas las filas
  ptrTempReg=-1;
  free(TempReg);TempReg=(char*)malloc(tamReg);
  Uint i;char*ztmp;
  for(i=0;i<tReg;i++){
    ztmp=(char*)malloc(tamReg);
    memcpy(ztmp,Reg[i],ztamReg);
    bzero(ztmp+ztamReg,tamReg-ztamReg);
    free(Reg[i]);Reg[i]=ztmp;
  }
  return 0;
}

//====================================================================//
// ARCHIVOS                                                           //
//====================================================================//

/** ABRIR ARCHIVO DE BD
 * Para este almacenamiento se debe omitir la Extension
 * @param char* Nombre Archivo
 * @return 0=OK / !=ERR
 * ERR:
 * -1 - Imposible abrir archivo
 */
int Tabla::OpenF(const char*zna){int i;
  if(nomarch)CloseF();
  nomarch=(char*)malloc(strlen(zna)+1);if(!nomarch)return -1;
  strcpy(nomarch,zna);
  // Lee DBS
  char zfla[strlen(nomarch)+5];sprintf(zfla,"%s.dbs",zna);
  int fl=open(zna,O_RDONLY);
  if(fl==-1){
    if(SaveF())return -1;
    return 0;
  }
  read(fl,&tReg,sizeof(tReg));
  read(fl,&tCampo,sizeof(tCampo));
  Campo=(_STR_Campo*)malloc(sizeof(_STR_Campo)*tCampo);
  if(!Campo){close(fl);tCampo=0;tReg=0;return -1;}
  for(i=0;i<tCampo;i++)read(fl,Campo+i,sizeof(_STR_Campo));
  char*tmp,*tmp2;long ttmp;
  read(fl,&ttmp,sizeof(ttmp));
  tmp=(char*)malloc(ttmp);
  read(fl,tmp,ttmp);
  close(fl);tamReg=0;
  for(i=0;i<tCampo;i++){
    tmp2=(char*)malloc(strlen(tmp+(long)Campo[i].n)+1);
    strcpy(tmp2,tmp+(long)Campo[i].n);Campo[i].n=tmp2;
    tmp2=(char*)malloc(strlen(tmp+(long)Campo[i].desc)+1);
    strcpy(tmp2,tmp+(long)Campo[i].desc);Campo[i].desc=tmp2;
    offCampo[i]=tamReg;
    switch(Campo[i].tp){
    case 0:case 3:tamReg+=4;break;
    case 1:tamReg+=1;break;
    case 2:tamReg+=2;break;
    case 4:tamReg+=Campo[i].tam;break;
    case 5:tamReg+=8;break;
    }
  }free(tmp);
  TempReg=(char*)malloc(tamReg);
  // Lee DBD
  sprintf(zfla,"%s.dbd",zna);
  fl=open(zna,O_RDONLY);if(fl==-1)return -1;
  for(i=0;i<tReg;i++){
    Reg[i]=(char*)malloc(tamReg);
    read(fl,Reg[i],tamReg);
  }
  close(fl);
  return 0;
}

int Tabla::SaveF(){int i;
  // Guarda DBS
  char zfla[strlen(nomarch)+5];sprintf(zfla,"%s.dbs",nomarch);
  int fl=open(zfla,O_WRONLY);if(fl==-1)return -1;
  write(fl,&tReg,sizeof(tReg));
  write(fl,&tCampo,sizeof(tCampo));
  char*tmp;long ttmp=0,ttmp2=0;
  for(i=0;i<tCampo;i++)ttmp+=strlen(Campo[i].n)+strlen(Campo[i].desc)+2;
  tmp=(char*)malloc(ttmp);
  { _STR_Campo ztmp;
    for(i=0;i<tCampo;i++){ztmp=Campo[i];
      strcpy(tmp+ttmp2,ztmp.n);ztmp.n=tmp+ttmp2;ttmp2+=strlen(Campo[i].n)+1;
      strcpy(tmp+ttmp2,ztmp.desc);ztmp.desc=tmp+ttmp2;
      ttmp2+=strlen(Campo[i].desc)+1;
      write(fl,&ztmp,sizeof(_STR_Campo));
    }
  }
  write(fl,&ttmp,sizeof(ttmp));
  write(fl,tmp,ttmp);
  close(fl);free(tmp);
  // Guarda DBD
  sprintf(zfla,"%s.dbd",zfla);
  fl=open(zfla,O_WRONLY);if(fl==-1)return -1;
  for(i=0;i<tReg;i++)write(fl,Reg[i],tamReg);
  close(fl);
  return 0;
}

void Tabla::CloseF(){_Free();
  if(nomarch)free(nomarch);nomarch=0;
}

}}}
