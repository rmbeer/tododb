#include "cuenta.hpp"

namespace TodoSQL{

int BDD::ms_ifinit=0;

Cuenta::Cuenta(){
  ms_ver=mysql_get_client_info();
  ms_b=0;
}
Cuenta::~Cuenta(){
  if(ms_b)mysql_close(ms_b);
}

/** CONECTA CON MYSQL
 * @param char* IP (Ej.: 127.0.0.1)
 * @param Uint Puerto (0==3306)
 * @param char* Nombre usuario
 * @param char* Contraseña
 * @return 0=OK / !=ERR
 * ERR:
 * -1 - Ya esta conectado!
 * -2 - No se puede inicializar MySQL
 * -3 - No se puede conectar con MySQL
 */
int Cuenta::Connect(char*zip,Uint zprt,char*zusr,char*pass){
  //TODO: 'ms_b' funciona para todos o solo para la conexion abierta?
  if(ms_b)return -1;MYSQL*ms_b=mysql_init(0);if(!ms_b)return -2;
  if(!mysql_real_connect(ms_b,zip,zusr,zpass,0,(zprt)?zprt:3306,0,0)){return -3;
  return 0;
}

/** CREA UNA NUEVA BASE DE DATOS
 * @param char* Nombre de Base de Datos
 * @return 0=OK / !=ERR
 * ERR:
 * -1 - Sin memoria
 * -2 - Error con MySQL
 */
BDD*Cuenta::NewBDD(char*znbdd){
  char*tmp=asprintf(znbdd,"CREATE DATABASE %s",znbdd);
  if(!tmp)return -1;
  if(mysql_query(ms_b,tmp)){free(tmp);return -2;}free(tmp);
  return 0;
}

/** ABRE BASE DE DATOS EXISTENTE
 * @param char* Nombre de Base de Datos
 * @return 0=OK / !=ERR
 * ERR:
 * -1 - Sin memoria
 * -2 - Error con MySQL: Imposible enviar ordenes
 * -3 - Error con MySQL: Imposible tomar resultados
 */
BDD*Cuenta::GetBDD(char*znbdd){
  char*tmp=asprintf(znbdd,"SELECT * FROM %s",znbdd);
  if(!tmp)return -1;
  if(mysql_query(ms_b,tmp)){free(tmp);return -2;}free(tmp);
  MYSQL_RES*r=mysql_store_result(ms_b);
  if(!r)return -3;
  BDD*ztmp=new BDD(r);
  if(!ztmp)return -1;
  return 0;
}

}
