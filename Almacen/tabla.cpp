#include "tabla.hpp"

namespace TodoDB{
namespace Almacen{

Tabla::Tabla(){
}
Tabla::~Tabla(){
}

int Tabla::AddFila(){return 0;}
int Tabla::AddFila(Uchar,...){return 0;}
int Tabla::SetFila(){return 0;}
int Tabla::SetFila(Uchar,...){return 0;}

/** TOMA LA FILA ACTUAL Y LO ALMACENA EN LA FILA TEMPORAL
 * @return 0=OK / !=ERR
 */
int Tabla::GetFila(){
  return 0;
}

/** SALTA AL NUMERO DE FILA
 * @return 0=OK / !=ERR
 */
int Tabla::JmpFila(Ulong zp){
  return 0;
}

/** DEVUELVE TOTAL DE FILAS
 * @return !=Total de filas
 */
Ulong Tabla::GetTFila(){Ulong trow=0;
  return trow;
}

/** DEVUELVE TOTAL DE COLUMNAS
 * @return !=Total de columnas
 */
Uchar Tabla::GetTCol(){
  return 0;
}

//============================================================================//
// PUNTEROS                                                                   //
//============================================================================//

int Tabla::RegPrev(){
  return 0;
}

int Tabla::RegNext(){
  return 0;
}

int Tabla::RegFirst(){
  return 0;
}

int Tabla::RegLast(){
  return 0;
}

}}
