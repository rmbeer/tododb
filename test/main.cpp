#include <stdio.h>
#include "../Almacen/GallinaR/tabla.hpp"

void Mostrar(TodoDB::Almacen::GallinaR::Tabla&tbl){int i;
  printf("Nombre     |Edad |  Telefono\n");
  printf("==================================\n");
  for(i=0;i<tbl.GetTFila();i++){
    printf("%10s | %3d | %14s\n",tbl.DGetS(i,0),tbl.DGetN(i,1),tbl.DGetS(i,2));
  }printf("\n");
}

int main(){
  TodoDB::Almacen::GallinaR::Tabla tbl;
  tbl.OpenF("prueba");
  //Crear tabla
  tbl.CAdd("Nombre",4,30,"Nombre del contacto.");
  tbl.CAdd("Edad",1,0,"Edad.");
  tbl.CAdd("Tel",4,14,"Numero de telefono.");
  tbl.AddFila(0,"Mike",22,"4773-3387");
  tbl.AddFila(0,"Emanuel",15,"2387-2787");
  tbl.AddFila(0,"Leandro",30,"8953-8437");
  tbl.AddFila(0,"Trololol",40,"8573-3787");
  printf("CREAR:\n");
  Mostrar(tbl);
  //Cambiar un registro
  printf("Edad de Leandro: %d\n",tbl.DGetNn(2,"Edad"));
  tbl.JmpFila(2);
  tbl.GetFila();
  tbl.DSetn("Edad",35);
  tbl.DSetn("Nombre","Mauricio");
  tbl.SetFila();
  printf("MODIFICAR:\n");
  Mostrar(tbl);
  //Agregar nuevo registro
  printf("AGREGAR:\n");
  tbl.DSetn("Nombre","Marta");
  tbl.AddFila();
  Mostrar(tbl);
  //Duplicar 1 registro
  printf("DUPLICAR:\n");
  tbl.JmpFila(3);
  tbl.GetFila();
  tbl.AddFila();
  Mostrar(tbl);
  tbl.CloseF();
  return 0;
}
