#include "../../../../Log/Log.h"
#include "Sql.hpp"

#ifdef DEBUG
#define _P "[IRC-SQL] "
#else
#define _P
#endif

namespace SQL{
namespace IRC{

MAIN::MAIN(),con(0){ifcon=0;
}
MAIN::~MAIN(){
  if(ifcon){;}
}

/** INICIA LA CONEXION
 * @return 0=OK / !=ERR
 */
int MAIN::Init(){
  if(!con.connect(IRCSQL_BD,IRCSQL_HOST,IRCSQL_USR,IRCSQL_PASS)){
    syslog(LOG_ERR,_P"Imposible conectarse a MySQL");return -1;}
  ifcon=-1;return 0;
}

}}
