#ifndef __TODOSQL_IRC_LOG__
#define __TODOSQL_IRC_LOG__
#include <mysql++.h>

namespace SQL{
namespace IRC{

class Log{
public:
  Log();
  ~Log();
  int Add(Uint,char*,char*,char*,char*);
  TodoBD::BDD bdd;
  TodoBD::Tabla tbl;
};

}}

#endif
