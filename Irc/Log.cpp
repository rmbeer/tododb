#include "../../../../Log/Log.h"
#include "../../../../Com/Base.h"
#include "IrcSql.hpp"

#ifdef DEBUG
#define _P "[IRC-SQL] "
#else
#define _P
#endif

namespace SQL{
namespace IRC{

Log::Log(){
}
Log::~Log(){
}

/** INICIALIZA LOG IRC
 * @return 0=OK / !=ERR
 * ERR:
 * -1 - Archivo BD no se puede abrir
 * -2 - Tabla no existe
 */
int Log::Init(){
  int i,fl=bdd.Open("irc-log.tbd");
  if(!fl)return -1;
  i=bdd.GetTabla(&tbl,"Log");
  if(i)return -2;
  return 0;
}

/** AGREGA LINEA IRC
 * Guarda el comando, canal, nombre y linea de texto en la base de datos
 * @param Uint Numero de Comando
 * @param char* Nombre canal
 * @param char* Nombre usuario
 * @param char* Host usuario
 * @param char* Linea de texto
 * @return 0=OK / !=ERR
 */
int Log::Add(Uint ncmd,char*zcanal,char*znm,char*zhost,char*ztxt){
  tbl.RegAdd(ncmd,zcanal,znm,zhost,ztxt);
  return 0;
}

int Log::GetLog(char*znm){
  tbl.
  return 0;
}

}}
