# TodoSQL

Escrito enteramente en C++, es un acceso directo a la base de datos cuyo proposito es ofrecer una API en C++ ahorrando todos los problemas y dolores de cabeza de MySQL++. Hace falta un manejo mas sencillo y sin problemas de seguridad, y que el manejo de un puerto en red no sea una obligacion.
El usuario puede elegir el Motor de DB que desee.

Metas del repositorio:

- Sin puertos de red abiertos
- Sin secuencias de SQL (elimina con esto toda inseguridad)
- Acceso directo a la DB
- Uso de todos los motores de DB
- Accesible desde Python, Java, y principalmente en C/C++

Ventajas reales:
	Centrado en la API y no en cualquier motor usado. Con esto elimina toda posibilidad de ser un ORM. NO ES UN ORM! Con esto aclarado, usted codea con cualquier DB solo preocupandose por su codigo en mas de un sentido. Sin importar si es C/C++, Python o Java, en la API de cada lenguaje debe estar especificamente diseñado para su lenguaje.
	Cualquier diseño de DB es la menor de las preocupaciones, en menos de 10 minutos puede diseñar una simple DB y el resto dedicarse a programar en su lenguaje solo utilizando los datos de la DB en el fondo.

Documentaciones con Licencia CC: BY-NC-SA
Codigos Fuentes con Licencia GPL
